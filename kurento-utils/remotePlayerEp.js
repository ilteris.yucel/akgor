const utils = require('../lib/customUtil.js');
const wsUtil = require('../lib/wsUtil.js');
module.exports = class RemotePlayerEp {
    constructor(pipeline, uri, user, ws){
        this.pipeline = pipeline;
        this.uri = uri;
        this.user = user;
        this.recordUnit = null;
        this.eventListenerStatus = false;
        this.ep = null;
        this.ws = ws;
        this.params = {
            uri: this.uri,
            useEncoding: true
        };
        this.name = 'RemotePlayerEndpoint-' + utils.uid();
    }
    getRecordUnit(){
        return this.recordUnit;
    }
    async setRecordUnit(recordUnit){
        this.recordUnit = recordUnit;
        await this.ep.connect(this.recordUnit.ep, 'VIDEO');
    }
    async create(){
        this.ep = await this.pipeline.create('PlayerEndpoint', this.params);
        await this.ep.setName(this.name);
        this.user.setRemotePlayerEndpoint(this);
    }
    async addEventListeners(){
        const name = await this.ep.getName();
        console.info(`[Add Base Event Listeners on RemotePlayerEndPoint] name : ${name} sessionId : ${this.user.sessionId}`);
        console.info(`[Adding Error Event on ${name}]`);
        this.ep.on('Error', (event) => {
            console.error(`On RemotePlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} description: ${event.description} errorCode: ${event.errorCode}`);
        });
        console.info(`[Adding MediaFlowInStateChange Event on RemotePlayerEndpoint]`);
        this.ep.on('MediaFlowInStateChange', (event) => { 
            console.info(`On RemotePlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding MediaFlowOutStateChange Event on RemotePlayerEndpoint]`);
        this.ep.on('MediaFlowOutStateChange', async (event) => {
            console.info(`On RemotePlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
            if(!this.recordUnit){
                console.info(`RecordUnit of ${name} does not exist`);
            }else{
                event.state === 'FLOWING' ? await this.recordUnit.record() : await this.recordUnit.pause();        
            }
        });
        console.info(`[Adding MediaTranscodingStateChange Event on RemotePlayerEndpoint]`);
        this.ep.on('MediaTranscodingStateChange', (event) => {
            console.info(`On RemotePlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} bindName: ${event.bindName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding EndOfStream Event on RemotePlayerEndpoint]`);
        this.ep.on('EndOfStream', (event) => {
            console.info(`On RemotePlayerEndpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
            this.release();
        });
        this.eventListenerStatus = true;
    }
    async release(){
        const message = {
            'id': 'REMOTE_STREAM_STOP'
        };
        if(this.ep){
            console.log("RemotePlayerEp releasing...");
            await this.ep.release();
        }
        if(this.recordUnit.ep){
            await this.recordUnit.release();
        }
        this.ep = null;
        this.user.setRemotePlayerEndpoint(null);
        wsUtil.sendMessage(this.ws, message);
    }
    async play(){
        if(this.recordUnit && this.eventListenerStatus){
            await this.ep.play()
        }else{
            const name = await this.ep.getName();
            console.info(`RemotePlayerEndpoint ${name} recordUnit is ${this.recordUnit} eventListenerStatus is ${this.eventListenerStatus}`);
        }
    }
    async pause(){
        await this.ep.pause();
    }
}