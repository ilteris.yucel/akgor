const wsUtil = require('../lib/wsUtil.js');
const kurento = require('kurento-client');
module.exports = async function(ws, sessionId, webRtcEp, resolution){
    let name = await webRtcEp.getName();
    let id = webRtcEp.id;

    console.info(`[Add webRtc Events Listener on ] name: ${name} id: ${id} sessionId: ${sessionId}`);

    console.info(`[Adding OnIceCandidate Event on WebRtcEP]`);
    webRtcEp.on('OnIceCandidate', (event) => {
        //console.info(`On WebRTC Endpoint type : ${event.type}  source : ${event.source} timestamp : ${event.timestamp} tags : ${event.tags}, candiate : ${JSON.stringify(event.candidate)}`);
        let candiate = kurento.getComplexType('IceCandidate')(event.candidate);
        let message = {
            'id' : 'ADD_ICE_CANDIDATE',
            'webRtcEp' : id,
            'candidate' : event.candidate,
            'resolution' : resolution
        }
        wsUtil.sendMessage(ws, message);
    });

    console.info(`[Adding IceCandidateFound Event on WebRtcEP]`);
    webRtcEp.on('IceCandidateFound', (event) => {
        //console.info(`On WebRTC Endpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} candiate: ${JSON.stringify(event.candidate)}`);
        let candiate = kurento.getComplexType('IceCandidate')(event.candidate);
        let message = {
            'id' : 'ADD_ICE_CANDIDATE',
            'webRtcEp' : id,
            'candidate' : event.candidate,
            'resolution' : resolution
        }
        wsUtil.sendMessage(ws, message);
    });


    console.info(`[Adding IceComponentStateChange Event on WebRtcEP]`);
    webRtcEp.on('IceComponentStateChange', (event) => {
        console.log(`On WebRTC Endpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} streamId: ${event.streamId} componentId: ${event.componentId} state: ${event.state}`);                
    });

    console.info(`[Adding IceGatheringDone Event on WebRtcEP]`);
    webRtcEp.on('IceGatheringDone', (event) => {
        console.info(`On WebRTC Endpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} `);                
    });

    console.info(`[Adding NewCandidatePairSelected Event on WebRtcEP]`);
    webRtcEp.on('NewCandidatePairSelected', (event) => {
        //console.info(`On WebRTC Endpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} streamId: ${event.candidatePair.streamId} localCandiate: ${event.candidatePair.localCandidate} remoteCandiate: ${event.candidatePair.remoteCandidate}`);                
    });
}