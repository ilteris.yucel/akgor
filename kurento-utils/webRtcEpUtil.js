const addBaseEventListeners = require('./addBaseEventListeners.js');
const addWebRtcEventListeners = require('./addWebRtcEventListeners.js'); 
const wsUtil = require('../lib/wsUtil.js');
const maxBitrate = {
    '480': 1200,
    '720': 4000,
    '1080': 8000
};

module.exports = {
    initWebRtcEndpoint: async function(ws, sessionId, webRtcEp, sdpOffer, resolution){
        //console.log("Coming SdpOffer" + sdpOffer);
        await addBaseEventListeners(sessionId, webRtcEp, 'WebRtcEndpoint');
        await addWebRtcEventListeners(ws, sessionId, webRtcEp, resolution);
        await webRtcEp.setMaxOutputBitrate(maxBitrate[resolution]);
        const name = await webRtcEp.getName();
        const sdpAnswer = await webRtcEp.processOffer(sdpOffer);
        const message = {
            'id': 'PROCESS_SDP_ANSWER_PLAYER_EP',
            'sdpAnswer': sdpAnswer,
            'resolution': resolution
        }
        wsUtil.sendMessage(ws, message);
    },
    startWebRtcEndpoint: async function(webRtcEp){
        console.log("Gathering Canddiates");
        await webRtcEp.gatherCandidates();   
    }
}