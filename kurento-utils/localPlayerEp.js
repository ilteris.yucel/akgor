const utils = require('../lib/customUtil.js');
const wsUtil = require('../lib/wsUtil.js');
module.exports = class localPlayerEp {
    constructor(pipeline, uri, user, ws){
        this.pipeline = pipeline;
        this.uri = uri;
        this.user = user;
        this.rtcUnits = {
            '480': null,
            '720': null,
            '1080': null
        };
        this.ep = null;
        this.ws = ws;
        this.eventListenerStatus = false;
        this.pauseTimeout = 0;
        this.params = {
            uri: this.uri,
            useEncoding: true
        };
        this.name = 'LocalPlayerEndpoint-' + utils.uid();
    }
    getRtcUnit(reso){
        return this.rtcUnits[reso];
    }
    getRtcUnits(){
        return this.rtcUnits;
    }
    getEp(){
        return this.ep;
    }
    async setRtcUnit(rtcUnit){
        this.rtcUnits[rtcUnit.resolution] = rtcUnit;
        await this.ep.connect(this.rtcUnits[rtcUnit.resolution].ep);
    }
    async create(){
        this.ep = await this.pipeline.create('PlayerEndpoint', this.params);
        await this.ep.setName(this.name);
        this.user.setLocalPlayerEndpoint(this);
    }
    async addEventListeners(){
        const name = await this.ep.getName();
        console.info(`[Add Base Event Listeners on LocalPlayerEndpoint] name : ${name} sessionId : ${this.user.sessionId}`);
        console.info(`[Adding Error Event on ${name}]`);
        this.ep.on('Error', (event) => {
            console.error(`On LocalPlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} description: ${event.description} errorCode: ${event.errorCode}`);
        });
        console.info(`[Adding MediaFlowInStateChange Event on LocalPlayerEndpoint]`);
        this.ep.on('MediaFlowInStateChange', (event) => { 
            console.info(`On LocalPlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding MediaFlowOutStateChange Event on LocalPlayerEndpoint]`);
        this.ep.on('MediaFlowOutStateChange', (event) => {
            console.info(`On LocalPlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding MediaTranscodingStateChange Event on LocalPlayerEndpoint]`);
        this.ep.on('MediaTranscodingStateChange', (event) => {
            console.info(`On LocalPlayerEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} bindName: ${event.bindName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding EndOfStream Event on LocalPlayerEndpoint]`);
        this.ep.on('EndOfStream', (event) => {
            console.info(`On LocalPlayerEndpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
            this.handleEOF();
        });
        this.eventListenerStatus = true;
    }
    async pause(){
        const message = {
            'id': 'PAUSE'
        };
        await this.ep.pause();
        wsUtil.sendMessage(this.ws, message);
    }
    async resume(){
        const message = {
            'id': 'RESUME'
        };
        await this.ep.play();
        wsUtil.sendMessage(this.ws, message);
    }
    async handleEOF(){
        await this.pause();
        await utils.timeout(4000);
        await this.resume();
    }
    async release(){
        const message = {
            'id': 'LOCAL_STREAM_STOP'
        };
        const unitKeys = Object.keys(this.rtcUnits);
        if(this.ep){
            console.log('LocalPlayerEndpoint releasing...');
            await this.ep.release();
        }
        if(this.rtcUnits){
            unitKeys.forEach(async (unit) => {
                await this.rtcUnits[unit].release();
            })
        } 
        this.ep = null;
        this.user.setLocalPlayerEndpoint(null);
        this.rtcUnits = this.user.default;
        this.user.setAllWebrtcEndpoint(this.user.default);
        clearTimeout(this.pauseTimeout);
        wsUtil.sendMessage(this.ws, message);
    }
    async play(){
        //TODO: Check rtcUnit is not null
        if(this.eventListenerStatus){
            await this.ep.play()
        }else{
            const name = await this.ep.getName();
            console.info(`LocalPlayerEndpoint ${name} rtcUnit is ${this.rtcUnit} eventListenerStatus is ${this.eventListenerStatus}`);
        }
    }
    async getVideoInfo(){
        const videoInfo = await this.ep.getVideoInfo();
        const message = {
            'id': 'VIDEO_INFO',
            'isSeekable': videoInfo.isSeekable,
            'initSeekable': videoInfo.initSeekable,
            'endSeekable': videoInfo.endSeekable,
            'videoDuration': videoInfo.duration,
        };
        console.log(message);
        wsUtil.sendMessage(this.ws, message);      
    }
}
