const addBaseEventListeners = require('./addBaseEventListeners');
const addRecorderEventListeners = require('./addRecorderEventListeners');
module.exports = {
    initRecorderEndpoint: async function(ws, sessionId, recorderEp){
        await addBaseEventListeners(sessionId, recorderEp, 'RecorderEndpoint');
        await addRecorderEventListeners(ws, sessionId, recorderEp);
    }
}