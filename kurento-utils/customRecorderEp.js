const utils = require('../lib/customUtil.js');
const wsUtil = require('../lib/wsUtil.js');
module.exports = class CustomRecorderEp{
    constructor(pipeline, uri, user, ws){
        this.pipeline = pipeline;
        this.uri = uri;
        this.user = user;
        this.ep = null;
        this.ws = ws;
        this.eventListenerStatus = false;
        this.params = {
            uri: this.uri,
            mediaProfile: 'WEBM_VIDEO_ONLY',
            stopOnEndOfStream: true
        }
        this.playerUnit = null;
        this.name = 'CustomRecorderEndpoint-' + utils.uid();
    }
    async create(){
        this.ep = await this.pipeline.create('RecorderEndpoint', this.params);
        await this.ep.setName(this.name);
        this.user.setRecorderEndpoint(this);
    }
    setPlayerUnit(playerUnit){
        this.playerUnit = playerUnit;
    }
    getPlayerUnit(){
        return this.playerUnit;
    }
    async addEventListeners(){
        const name = await this.ep.getName();
        console.info(`[Add Base Event Listeners on CustomRecorderEndpoint] name : ${name} sessionId : ${this.user.sessionId}`);
        console.info(`[Adding Error Event on ${name}]`);
        this.ep.on('Error', (event) => {
            console.error(`On CustomRecorderEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} description: ${event.description} errorCode: ${event.errorCode}`);
        });
        console.info(`[Adding MediaFlowInStateChange Event on CustomRecorderEndpoint]`);
        this.ep.on('MediaFlowInStateChange', async (event) => { 
            console.info(`On CustomRecorderEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
            //event.type === 'FLOWING' ? await this.record() : await this.pause(); 
        });
        console.info(`[Adding MediaFlowOutStateChange Event on CustomRecorderEndpoint]`);
        this.ep.on('MediaFlowOutStateChange', (event) => {
            console.info(`On CustomRecorderEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding MediaTranscodingStateChange Event on CustomRecorderEndpoint]`);
        this.ep.on('MediaTranscodingStateChange', (event) => {
            console.info(`On CustomRecorderEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} bindName: ${event.bindName} mediaType: ${event.mediaType}`);
        });
        console.info(`[Adding Recording Event on CustomRecorderEndpoint]`);
        this.ep.on('Recording', async function(event){
            console.info(`On CustomRecorderEndpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
            if(!this.playerUnit){
                console.info(`CustomRecorderEndpoint name: ${name} playerUnit is null`);
            }else{
                console.info(`${this.playerUnit.name} play in 4 seconds...`);
                await utils.timeout(4000);
                await this.playerUnit.play();
            }
        }.bind(this));
        console.info(`[Adding Stopped Event on CustomRecorderEndpoint]`);
        this.ep.on('Stopped', function(event){
            console.info(`On CustomRecorderEndpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
        });
        console.info(`[Adding Paused Event on CustomRecorderEndpoint]`);
        this.ep.on('Paused', function(event){
            console.info(`On CustomRecorderEndpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
        });
        this.eventListenerStatus = true;

    }
    async record(){
        console.info("Recording..");
        if(this.eventListenerStatus) this.ep.record();
        else{
            const name = await this.ep.getName();
            console.info(`CustomRecorderEndpoint name: ${name} EventListenerStatus is ${this.eventListenerStatus}`); 
        }
    }
    async pause(){
        console.info("Pausing...");
        if(this.eventListenerStatus) this.ep.stopAndWait();
        else{
            const name = await this.ep.getName();
            console.info(`CustomRecorderEndpoint name: ${name} EventListenerStatus is ${this.eventListenerStatus}`); 
        }
    }
    async release(){
        if(this.ep){
            console.log("RecorderEp releasing...");
            await this.ep.release();
            this.ep = null;
            this.user.setRecorderEndpoint(null);
        }
    }
}