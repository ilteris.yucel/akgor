module.exports = async function(sessionId, endpoint, epName){
    let name = await endpoint.getName();
    console.info(`[Add Base Event Listeners on] name : ${name} class:${epName} sessionId : ${sessionId}`);

    console.info(`[Adding Error Event on ${epName}]`);
    endpoint.on('Error', (event) => {
        console.error(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} description: ${event.description} errorCode: ${event.errorCode}`);
    });

    console.info(`[Adding MediaFlowInStateChange Event on ${epName}]`);
    endpoint.on('MediaFlowInStateChange', (event) => { 
        console.info(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        if(epName === 'RecorderEndpoint'){
            if(event.state === 'FLOWING'){
                console.info(`[Recording start on RecorderEndpoint] when ${event.timestamp}`);
            }else if(event.state === 'NOT_FLOWING'){
                console.info(`[Recording stop on RecorderEndpoint] when ${event.timestamp}`)
            }
            
        }
    });

    console.info(`[Adding MediaFlowOutStateChange Event on ${epName}]`);
    endpoint.on('MediaFlowOutStateChange', (event) => {
        console.info(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        if(epName === 'PlayerEndpoint'){
            const recorder = endpoint.recordUnit;
            if(recorder && event.state === 'FLOWING'){
                recorder.record();
            }else if(recorder && event.state === 'NOT_FLOWING'){
                recorder.stopAndWait();
            }
        }
    });
            
    console.info(`[Adding ConnectionStateChanged Event on ${epName}]`);
    if(epName === 'WebRtcEndpoint'){
        endpoint.on('ConnectionStateChanged', (event) => {
            console.info(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} oldState: ${event.oldState} newState: ${event.newState}`);
        });
    }
    console.info(`[Adding MediaStateChanged Event on ${epName}]`);
    if(epName === 'WebRtcEndpoint'){
        endpoint.on('MediaStateChanged', (event) => {
            console.info(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} oldState: ${event.oldState} newState: ${event.newState}`);
        });
    }
    console.info(`[Adding MediaTranscodingStateChange Event on ${epName}]`);
    endpoint.on('MediaTranscodingStateChange', (event) => {
        console.info(`On ${epName} event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} bindName: ${event.bindName} mediaType: ${event.mediaType}`);
    });
}