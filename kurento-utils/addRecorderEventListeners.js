const wsUtil = require('../lib/wsUtil.js');
module.exports = async function(ws, sessionId, recorderEp){
    let name = await recorderEp.getName();
    
    console.info(`[Adding Recording Event on RecorderEp]`);
    recorderEp.on('Recording', function(event){
        console.info(`On Recorder Endpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
    });
    console.info(`[Adding Stopped Event on RecorderEp]`);
    recorderEp.on('Stopped', function(event){
        console.info(`On Recorder Endpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
    });
    console.info(`[Adding Paused Event on RecorderEp]`);
    recorderEp.on('Paused', function(event){
        console.info(`On Recorder Endpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
    });
}