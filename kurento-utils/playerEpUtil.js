const addBaseEventListeners = require("./addBaseEventListeners.js");
const addPlayerEndPointListeners = require('./addPlayerEventListeners.js');
const wsUtil = require('../lib/wsUtil.js');
module.exports = {
    initPlayerEndpoint: async function(ws, sessionId, playerEp){
        await addBaseEventListeners(sessionId, playerEp, 'PlayerEndpoint');
        await addPlayerEndPointListeners(ws, sessionId, playerEp);
    },
    getVideoInfo: async function(ws, playerEp){
        const videoInfo = await playerEp.getVideoInfo();
        const message = {
            'id': 'VIDEO_INFO',
            'isSeekable': videoInfo.isSeekable,
            'initSeekable': videoInfo.initSeekable,
            'endSeekable': videoInfo.endSeekable,
            'videoDuration': videoInfo.duration,
        };
        console.log(message);
        wsUtil.sendMessage(ws, message);      
    }
}