const utils = require('../lib/customUtil.js');
const wsUtil = require('../lib/wsUtil.js');
const kurento = require('kurento-client');
module.exports = class CustomWebRtcEp{
    constructor(pipeline, user, ws, resolution){
        this.pipeline = pipeline;
        this.user = user;
        this.ws = ws;
        this.resolution = resolution;
        this.ep = null;
        this.eventListenerStatus = false;
        this.name = 'CustomWebRtcEndpoint-' + utils.uid();
    }
    async create(){
        const resolutionObject = {
            '480': 1200,
            '720': 4000,
            '1080': 8000
        };
        this.ep = await this.pipeline.create('WebRtcEndpoint');
        await this.ep.setName(this.name);
        await this.ep.setMaxOutputBitrate(resolutionObject[this.resolution]);
        this.user.setWebRtcEndpoint(this, this.resolution);
    }
    async addEventListeners(){
        const name = await this.ep.getName();
        console.info(`[Add Base Event Listeners on CustomWebRtcEndpoint] name : ${name} sessionId : ${this.user.sessionId}`);
        console.info(`[Adding Error Event on ${name}]`);
        this.ep.on('Error', (event) => {
            console.error(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} description: ${event.description} errorCode: ${event.errorCode}`);
        });

        console.info(`[Adding MediaFlowInStateChange Event on ${name}]`);
        this.ep.on('MediaFlowInStateChange', (event) => { 
            console.info(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
            if(event.state === 'FLOWING'){
                const message = {
                    'id': 'MEDIA_FLOW_IN'
                };
                wsUtil.sendMessage(this.ws, message);
            }else{
                const message = {
                    'id': 'MEDIA_FLOW_OUT'
                };
                wsUtil.sendMessage(this.ws, message);                
            }
        });

        console.info(`[Adding MediaFlowOutStateChange Event on ${name}]`);
        this.ep.on('MediaFlowOutStateChange', (event) => {
            console.info(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} padName: ${event.padName} mediaType: ${event.mediaType}`);
        });
            
        console.info(`[Adding ConnectionStateChanged Event on ${name}]`);
        this.ep.on('ConnectionStateChanged', (event) => {
            console.info(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} oldState: ${event.oldState} newState: ${event.newState}`);
        });
        console.info(`[Adding MediaStateChanged Event on ${name}]`);
        this.ep.on('MediaStateChanged', (event) => {
            console.info(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} oldState: ${event.oldState} newState: ${event.newState}`);
        });
        console.info(`[Adding MediaTranscodingStateChange Event on ${name}]`);
        this.ep.on('MediaTranscodingStateChange', (event) => {
            console.info(`On CustomWebRtcEndpoint event :${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} state: ${event.state} bindName: ${event.bindName} mediaType: ${event.mediaType}`);
        });

        console.info(`[Adding OnIceCandidate Event on ${name}]`);
        this.ep.on('OnIceCandidate', (event) => {
            console.info(`On CustomWebRtcEnpoint type : ${event.type}  source : ${event.source} timestamp : ${event.timestamp} tags : ${event.tags}, candidate : ${JSON.stringify(event.candidate)}`);
            let candidate = kurento.getComplexType('IceCandidate')(event.candidate);
            let message = {
                'id' : 'ADD_ICE_CANDIDATE',
                'candidate' : candidate,
                'resolution' : this.resolution
            }
            wsUtil.sendMessage(this.ws, message);
        });
    
        console.info(`[Adding IceCandidateFound Event on ${name}]`);
        this.ep.on('IceCandidateFound', (event) => {
            console.info(`On CustomWebRtcEnpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} candiate: ${JSON.stringify(event.candidate)}`);
            let candidate = kurento.getComplexType('IceCandidate')(event.candidate);
            let message = {
                'id' : 'ADD_ICE_CANDIDATE',
                'candidate' : candidate,
                'resolution' : this.resolution
            }
            wsUtil.sendMessage(this.ws, message);
        })
        console.info(`[Adding IceComponentStateChange Event on ${name}]`);
        this.ep.on('IceComponentStateChange', (event) => {
            console.log(`On CustomWebRtcEnpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} streamId: ${event.streamId} componentId: ${event.componentId} state: ${event.state}`);                
        });
    
        console.info(`[Adding IceGatheringDone Event on ${name}]`);
        this.ep.on('IceGatheringDone', (event) => {
            console.info(`On CustomWebRtcEnpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} `);                
        });
    
        console.info(`[Adding NewCandidatePairSelected Event on ${name}]`);
        this.ep.on('NewCandidatePairSelected', (event) => {
            console.info(`On CustomWebRtcEnpoint type ${event.type} name: ${name} timestamp: ${event.timestamp} tags: ${event.tags} streamId: ${event.candidatePair.streamId} localCandiate: ${event.candidatePair.localCandidate} remoteCandiate: ${event.candidatePair.remoteCandidate}`);                
        });
    }
    async generateAnswer(sdpOffer){
        const sdpAnswer = await this.ep.processOffer(sdpOffer);
        const message = {
            'id': 'PROCESS_SDP_ANSWER_PLAYER_EP',
            'sdpAnswer': sdpAnswer,
            'resolution': this.resolution
        }
        wsUtil.sendMessage(this.ws, message);
    }
    async gatherCandidates(){
        await this.ep.gatherCandidates();
    }
    async release(){
        console.log('WebRtcEp releasing...');
        if(this.ep) await this.ep.release();
        this.user.setWebRtcEndpoint(null, this.resolution);
        this.ep = null;
    }
}