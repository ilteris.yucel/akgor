const wsUtil = require('../lib/wsUtil.js');
module.exports = async function(ws, sessionId, playerEp){ 
    let name = await playerEp.getName();

    console.info(`[Adding EndOfStream Event on PlayerEp]`);
    playerEp.on('EndOfStream', (event) => {
        console.info(`On Player Endpoint name: ${name} type: ${event.type}  timestamp: ${event.timestamp}`);
        const message = {
            'id': 'STOP'
        }
        wsUtil.sendMessage(ws, message);
    })
}