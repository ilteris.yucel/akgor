const https = require('https');
const fs = require('fs');
var path = require('path');
const cookieParser = require('cookie-parser')
const express = require('express');
const minimist = require('minimist');
const session = require('express-session');
const user = require('./lib/User.js');
const ws = require('ws');
const kurento = require('kurento-client');
const User = require('./lib/User.js');
const cors = require('cors');
const bodyParser = require("body-parser");
const playerEpUtil = require('./kurento-utils/playerEpUtil.js');
const webRtcEpUtil = require('./kurento-utils/webRtcEpUtil.js');
const wsUtil = require('./lib/wsUtil.js');
const utils = require('./lib/customUtil');
const recorderEpUtil = require('./kurento-utils/recorderEpUtil.js');
const RemotePlayerEp = require('./kurento-utils/remotePlayerEp.js');
const LocalPlayerEp = require('./kurento-utils/localPlayerEp.js');
const CustomWebRtcEp = require('./kurento-utils/customWebRtcEp.js');
const CustomRecorderEp = require('./kurento-utils/customRecorderEp.js');

let users = {};
let candidatesQueue = {};
let kurentoClient = null;

const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
}
const argv = minimist(process.argv.slice(2), {
    default: {
        as_uri: 'https://localhost:8443',
        ws_uri: 'ws://localhost:8888/kurento',
        react_uri : 'https://localhost:3000/',
        client_uri: 'wss://localhost:8443/media-server'
    }
});
const app = express();
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const sessionHandler = session({
    secret : 'none',
    rolling : true,
    resave : true,
    saveUninitialized : true
});
app.use(sessionHandler);

const asUrl = new URL(argv.as_uri);
const hostname = asUrl.hostname;
const port = asUrl.port;

const server = https.createServer(options, app, (req, res) => {
    res.send("OK");
});

server.listen(port, hostname, () => {
    console.log(`Server running at https://${hostname}:${port}/`);
});

server.on('error', (error) =>{
    if(error.syscall !== 'listen'){
        throw error;
    }
    let bind = typeof port === "string" ? "Pipe :" + port : "Port :" + port;
    switch(error.code){
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
});

server.on('listening', () => {
    var addr = server.address();
    var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
    console.log("Listening on " + bind);
})

app.get('/get-json', (req, res) => {
    let rawData = fs.readFileSync('./assets/data/stream.json');
    let streamList = JSON.parse(rawData);
    res.json(streamList);
});

app.post('/post-json', (req, res) => {
    const streamInfoJSON = req.body.streamInfoJSON;
    const parsedJSON = JSON.stringify(streamInfoJSON);
    console.log(streamInfoJSON);
    fs.writeFileSync('./assets/data/stream.json', parsedJSON);
    res.send('File Updated');

})


const wss = new ws.Server({
    noServer: true,
    path: '/media-server'
});
server.on('upgrade', (req, socket, head) => {
    console.log("in server upgrade");
    wss.handleUpgrade(req, socket, head, (ws) => {
        console.log("in handleUpgrade");
        wss.emit('connection', ws, req);
    });
});
wss.on('connection', (ws, req) => {
    let sessionId = null;
    let request = req;
    let response = {};
    sessionHandler(request, response, (error) => {
        if(error){
            console.log(error);
            return error.message;
        }
        sessionId = request.session.id;
        console.log('Connection received with sessionId ' + sessionId);
    });
    ws.on('error', (error) => {
        console.log('Connection ' + sessionId + ' error');
    });

    ws.on('close', () => {
        wss.clients.delete(ws);
        console.log('Connection ' + sessionId + ' closed');        
    });

    ws.on('message', async (_message) => {
        const message = JSON.parse(_message);
        const messageId = message.id;
        const sdpOffer = message.sdpOffer;
        const uri = message.uri;
        const resolution = message.resolution;
        const filename = message.filename;
        const flag = message.firstflag;
        switch(messageId) {
            case 'PROCESS_SDP_OFFER_PLAYER_EP':
                console.log('Kurento Client Starting...');
                //await playerEpStart(sessionId, ws, sdpOffer, uri, resolution);
                await startStream(sessionId, ws, sdpOffer, uri, filename, resolution, flag);
                console.log('Kurento Clien Retrieving...');
                break;
            case 'STOP':
                console.log('Kurento Client Stopping...');
                await stop(ws, sessionId);
                break;
            case 'ADD_ICE_CANDIDATE':
                onIceCandidate(sessionId, message.candidate, message.resolution);
                break;
            case 'PAUSE':
                await pause(ws, sessionId);
                break;
            case 'RESUME':
                await resume(ws, sessionId);
                break;
            default:
                console.log('Something Wrong...');
                answer = {
                    'id' : 'ERROR'
                }
                wsUtil.sendMessage(ws, answer);
                break;
        }
    });
});



function onIceCandidate(sessionId, _candidate, resolution) {
    var candidate = kurento.getComplexType('IceCandidate')(_candidate);

    if (users[sessionId]) {
        console.info('Sending candidate');
        let webRtcEndpoint = users[sessionId].getWebRtcEndpoint(resolution);
        if(webRtcEndpoint) webRtcEndpoint.ep.addIceCandidate(candidate);
    }
    else {
        console.info('Queueing candidate');
        if (!candidatesQueue[sessionId]) {
            candidatesQueue[sessionId] = [];
        }
        candidatesQueue[sessionId].push(candidate);
    }
}
function getKurentoClient(){
    if(kurentoClient !== null){
        return kurentoClient;
    }
    kurentoClient = new kurento(argv.ws_uri);
    return kurentoClient;
}
async function baseEpStructure(sessionId, ws, uri, filename, flag){
    if(flag){
        const recordFile = `file:///records/${filename}-${sessionId}.webm`;
        
        console.log("New User is creating...");
        const kurentoClient = getKurentoClient();
        const user = new User(kurentoClient, sessionId);
        users[sessionId] = user;

        console.log("Pipeline is creating...");
        const pipeline = await kurentoClient.create('MediaPipeline');
        user.setPipeline(pipeline);

        console.log("RemotePlayerEndpoint is creating...");
        const remotePlayerEp = new RemotePlayerEp(pipeline, uri, user, ws);
        await remotePlayerEp.create();

        console.log("CustomRecorderEndpoint is creating...");
        const customRecorderEp = new CustomRecorderEp(pipeline, recordFile, user, ws);
        await customRecorderEp.create();

        console.log('RemotePlayerEndpoint and CustomRecorderEndpoint are connecting...');
        await remotePlayerEp.setRecordUnit(customRecorderEp);

        console.log("LocalPlayerEndpoint is creating...");
        const localPlayerEp = new LocalPlayerEp(pipeline, recordFile, user, ws);
        await localPlayerEp.create();

        console.log("CustomRecorderEndpoint's LocalPlayerEndpoint is setting...");
        customRecorderEp.setPlayerUnit(localPlayerEp);

        console.log('Call addEventListeners on RemotePlayerEndpoint...');
        await remotePlayerEp.addEventListeners();

        console.log('Call addEventListeners on CustomRecorderEndpoint...');
        await customRecorderEp.addEventListeners();

        console.log('Call addEventListeners on LocalPlayerEndpoint...');
        await localPlayerEp.addEventListeners();

        console.log("RemotePlayerEndpoint is playing...");
        remotePlayerEp.play();
    }
}

async function addWebRtcEndpoint(sessionId, ws, sdpOffer, resolution){
    console.log(`CustomWebRtcEndpoint on ${resolution}P is creating...`);
    const user = users[sessionId];
    const pipeline = user.getPipeline();
    const localPlayerEp = user.getLocalPlayerEndpoint();

    const customWebRtcEp = new CustomWebRtcEp(pipeline, user, ws,resolution);
    await customWebRtcEp.create();

    console.log(`LocalPlayerEndpoint and CustomWebRtcEnpoint ${resolution}P are connecteing...`);
    await localPlayerEp.setRtcUnit(customWebRtcEp);

    console.log(`Call addEventListeners on CustomWebRtcEp ${resolution}P`);
    await customWebRtcEp.addEventListeners();

    console.log(`Generating SDP Answer on CustomWebRtcEndpoint ${resolution}P...`);
    await customWebRtcEp.generateAnswer(sdpOffer);
    console.log(`Gathering candidates on CustomWebRtcEndpoint ${resolution}P...`);
    await customWebRtcEp.gatherCandidates();
}
async function startStream(sessionId, ws, sdpOffer, uri, filename, resolution, flag){
    if(!sessionId){
        let message = {
            'id': 'ERROR',
            'desc': 'Undefined session id'
        }
        wsUtil.sendMessage(ws, message);        
    }
    /*const recordedFile = `file:///records/${filename}-${sessionId}.webm`;
    let user, kurentoClient;
    let rpepFlag, crepFlag, lpepFlag, cwepFlag;
    console.log('User Detecting...');
    user = users[sessionId];
    if(!user){
        console.log('User not found, creating...');
        kurentoClient = getKurentoClient();
        user = new User(kurentoClient, sessionId);
        users[sessionId] = user;
    }else{
        console.log('User found, getting...');
        kurentoClient = user.getKurentoClient();
    }

    let pipeline = user.getPipeline();
    if(!pipeline){
        console.log('Pipeline creating...');
        pipeline = await kurentoClient.create('MediaPipeline');
        user.setPipeline(pipeline);
    }
    let remotePlayerEp = user.getRemotePlayerEndpoint();
    if(!remotePlayerEp){
        console.log('RemotePlayerEndpoint creating...');
        remotePlayerEp = new RemotePlayerEp(pipeline, uri, user, ws);
        await remotePlayerEp.create();
        rpepFlag = true;
    }
    let customRecorderEp = user.getRecorderEndpoint();
    if(!customRecorderEp){
        console.log('RecorderEndpoint creating...');
        customRecorderEp = new CustomRecorderEp(pipeline, recordedFile, user, ws);
        await customRecorderEp.create();
        crepFlag = true;
    }
    if(rpepFlag || crepFlag){
        console.log('RemotePlayerEp and RecorderEp connecting...');
        await remotePlayerEp.setRecordUnit(customRecorderEp);
    }
    if(rpepFlag){
        console.log('Call addEventListeners on RemotePlayerEp');
        await remotePlayerEp.addEventListeners();
    }
    if(crepFlag){
        console.log('Call addEventListeners on RecorderEp');
        await customRecorderEp.addEventListeners();
    }
    if(rpepFlag){
        console.log('Play on RemotePlayerEp');
        await remotePlayerEp.play();
    }
    let localPlayerEp = user.getLocalPlayerEndpoint();
    if(!localPlayerEp){
        console.log('LocalPlayerEndpoint creating...');
        localPlayerEp = new LocalPlayerEp(pipeline, recordedFile, user, ws);
        await localPlayerEp.create();
        lpepFlag = true;
    }

    let customWebRtcEp = user.getWebRtcEndpoint(resolution);
    if(!customWebRtcEp){
        console.log(`CustomWebRtcEndpoint on ${resolution} creating...`);
        customWebRtcEp = new CustomWebRtcEp(pipeline, user, ws,resolution);
        await customWebRtcEp.create();
        cwepFlag = true;
    }
    if(lpepFlag || cwepFlag){
        console.log('LocalPlayerEp and WebRtcEp connecting...');
        await localPlayerEp.setRtcUnit(customWebRtcEp);
    }
    if(lpepFlag){
        console.log('Call addEventListeners on LocalPlayerEp');
        await localPlayerEp.addEventListeners();
    }
    if(cwepFlag){
        console.log('Call addEventListeners on CustomWebRtcEp');
        await customWebRtcEp.addEventListeners();
        console.log('Generating Answer on CustomWebRtcEp');
        await customWebRtcEp.generateAnswer(sdpOffer);
        await customWebRtcEp.gatherCandidates();
    }
    if(lpepFlag){
        console.log('Play on LocalPlayerEp');
        await localPlayerEp.play();
    }*/
    await baseEpStructure(sessionId, ws, uri, filename, flag);
    await addWebRtcEndpoint(sessionId, ws, sdpOffer, resolution);
    wsUtil.sendMessage(ws, {'id': 'STATE_CHANGE'});

}

async function hierarchicalStart(sessionId, ws, sdpOffer, uri, filename, resolution){
    //const recordFileName = `https://localhost:8443/records/${filename}.webm`;
    const recordFileName = `file:///records/${filename}.webm`;
    const mediaProfile = 'WEBM_VIDEO_ONLY';
    let remotePEpFlag = false,
        localPEpFlag = false,
        recordREpflag = false;
    if(!sessionId){
        const message = {
            'id': 'ERROR',
            'desc': 'Undefined session id'
        }
    }

    console.log('User Detecting...');
    let user, kurentoClient;

    user = users[sessionId];
    if(!user){
        console.log('User not found, created...')
        kurentoClient = getKurentoClient();
        user = new User(kurentoClient, sessionId);
        users[sessionId] = user;
    }else{
        kurentoClient = user.getKurentoClient();
    }

    
    let pipeline = user.getPipeline();
    if(!pipeline){
        console.log('Pipeline Creating...');
        pipeline = await kurentoClient.create('MediaPipeline');
        user.setPipeline(pipeline);
    }

    
    let webRtcEp = user.getWebRtcEndpoint(resolution);
    if(!webRtcEp){
        console.log('WebRtcEp Creating...');
        webRtcEp = await pipeline.create('WebRtcEndpoint');
        user.setWebRtcEndpoint(webRtcEp, resolution);
        console.log('WebRtcEp Setted');
    }

    
    let localPlayerEp = user.getLocalPlayerEndpoint();
    if(!localPlayerEp){
        console.log('Local Player Endpoint Creating...');
        localPlayerEp = await pipeline.create('PlayerEndpoint', {uri: recordFileName, useEncoding: true});
        user.setLocalPlayerEndpoint(localPlayerEp);
        localPEpFlag = true;
    }

    
    let remotePlayerEp = user.getRemotePlayerEndpoint();
    if(!remotePlayerEp){
        console.log('Remote Player Endpoint Creating...');
        remotePlayerEp = await pipeline.create('PlayerEndpoint', {uri: uri, useEncoding: true});
        user.setRemotePlayerEndpoint(remotePlayerEp);
        remotePEpFlag = true;
    }

    
    let recorderEp = user.getRecorderEndpoint();
    if(!recorderEp){
        console.log('Recorder Endpoint Creating...');
        recorderEp = await pipeline.create('RecorderEndpoint', {uri: recordFileName, mediaProfile: mediaProfile, stopOnEndOfStream: true});
        user.setRecorderEndpoint(recorderEp);
        recordREpflag = true;
    }

    await remotePlayerEp.connect(recorderEp, 'VIDEO');
    remotePlayerEp.recordUnit = recorderEp;
    await localPlayerEp.connect(webRtcEp);

    if(remotePEpFlag){
        await playerEpUtil.initPlayerEndpoint(ws, sessionId, remotePlayerEp);
        await remotePlayerEp.play();
    }
    if(recordREpflag){
        await recorderEpUtil.initRecorderEndpoint(ws, sessionId, recorderEp);
        //recorderEp.record();
    }
    await webRtcEpUtil.initWebRtcEndpoint(ws, sessionId, webRtcEp, sdpOffer, resolution);
    await webRtcEpUtil.startWebRtcEndpoint(webRtcEp);
    
    if(localPEpFlag){
        await playerEpUtil.initPlayerEndpoint(ws, sessionId, localPlayerEp);
        await localPlayerEp.play();
    }
    wsUtil.sendMessage(ws, {'id': 'STATE_CHANGE'});
    
}

async function playerEpStart(sessionId, ws, sdpOffer, uri, resolution){
    if(!sessionId){
        const message = {
            'id' : 'ERROR',
            'desc' : 'Undefined session id'
        }
        wsUtil.sendMessage(ws, message);
        return;
    }
    console.log("Kurento Client Getting...");
    const kurentoClient = getKurentoClient();
    let user = users[sessionId];
    if(!user){
        user = new User(kurentoClient, sessionId);
        users[sessionId] = user;
    }else{
        user.setKurentoClient(kurentoClient);
    }
    
    let pipeline = user.getPipeline();
    if(!pipeline){
        console.log("Pipeline creating...");
        pipeline = await kurentoClient.create('MediaPipeline');
        user.setPipeline(pipeline);
    }
    console.log("WebRtcEp  creating...");
    const webRtcEp = await pipeline.create('WebRtcEndpoint');
    user.setWebRtcEndpoint(webRtcEp, resolution);
    console.log("WebRTC endpoint setted");
    /*if(candidatesQueue[sessionId]){
        while(candidatesQueue[sessionId].length){
            let candidate = candidatesQueue[sessionId].shift();
            webRtcEp.addIceCandidate(candidate);
        }
    }*/
    let playerEp = users[sessionId].getPlayerEndpoint();
    let playerEpFlag = false;
    if(!playerEp){
        console.log("PlayerEp creating....");
        playerEp = await pipeline.create('PlayerEndpoint', {uri: uri, useEncoding: true});
        user.setPlayerEndpoint(playerEp);
        playerEpFlag = true;
    }

    await playerEp.connect(webRtcEp);

    await webRtcEpUtil.initWebRtcEndpoint(ws, sessionId, webRtcEp, sdpOffer, resolution);
    await webRtcEpUtil.startWebRtcEndpoint(webRtcEp);
    if(playerEpFlag){
        await playerEpUtil.initPlayerEndpoint(ws, sessionId, playerEp);
        await playerEp.play();
    }
    wsUtil.sendMessage(ws, {'id': 'STATE_CHANGE'})
}
async function pause(ws, sessionId){
    //const remotePlayerEp = users[sessionId].getRemotePlayerEndpoint();
    //await remotePlayerEp.pause();
    const localPlayerEp = users[sessionId].getLocalPlayerEndpoint();
    await localPlayerEp.getVideoInfo();
    await localPlayerEp.pause();
    const message = {
        "id": "INFO",
        "info": `Video is paused on ${sessionId}`
    };
    wsUtil.sendMessage(ws, message);
}
async function resume(ws, sessionId) {
    //const remotePlayerEp = users[sessionId].getRemotePlayerEndpoint();
    //await remotePlayerEp.play();
    const localPlayerEp = users[sessionId].getLocalPlayerEndpoint();
    await localPlayerEp.getVideoInfo();
    await localPlayerEp.play();
    const message = {
        "id": "INFO",
        "info": `Video is resumed on ${sessionId}`
    };
    wsUtil.sendMessage(ws, message);    
}
async function start(sessionId, ws, sdpOffer, callback){
    if(!sessionId){
        return callback("Cannot use undefined session id.");
    }
    let kC = getKurentoClient();
    let user = users[sessionId];
    if(!user){
        user = new User(kC, sessionId);
        users[sessionId] = user;
    }else{
        user.setKurentoClient(kC);
    }
    let pipeline = await kC.create('MediaPipeline');
    user.setPipeline(pipeline);
    let webRtcEp = await pipeline.create('WebRtcEndpoint');
    user.setWebRtcEndpoint(webRtcEp);
    if(candidatesQueue[sessionId]){
        while(candidatesQueue[sessionId].length){
            let candidate = candidatesQueue[sessionId].shift();
            webRtcEp.addIceCandidate(candidate);
        }
    }

    let rtpEp = await makeRtpEndpoint(pipeline);
    user.setRtpEndpoint(rtpEp);
    await rtpEp.connect(webRtcEp);

    await initWebRtcEndpoint(ws, sessionId, webRtcEp, sdpOffer);
    await startWebRtcEndpoint(webRtcEp);

    await startRtpEndpoint(ws, sessionId, rtpEp);
}

function sendMessage(ws, _message){
    let message = JSON.stringify(_message);
    ws.send(message);
}

async function stop(ws, sessionId){
    let user = users[sessionId];
    if(user){
        let pipeline = user.getPipeline();
        pipeline.release();
        user.setPipeline(null);
        let remotePlayerEp = user.getRemotePlayerEndpoint();
        await remotePlayerEp.release();
        let localPlayerEp = user.getLocalPlayerEndpoint();
        await localPlayerEp.release();
        delete users[sessionId];
        delete candidatesQueue[sessionId];
    }
    const message = {
        'id': 'INFO',
        'info': 'Pipeline and Session Deleted'
    }
    sendMessage(ws, message);

}

function rtpEndpointConfig(useComedia = false, useAudio = false, useSrtp = false){
    let senderIp = "127.0.0.1";
    let senderRtpPortAudio = 5006;
    let senderRtpPortVideo = 5004;
    let senderSsrcAudio = 445566;
    let senderSsrcVideo = 112233;
    let senderCname = "ilteris@pirireis.com.tr";
    let senderCodecV = "H264";

    let sdpComediaAttr = "";
    if(useComedia){
        senderRtpPortAudio = 9;
        senderRtpPortVideo = 9;

        sdpComediaAttr = "a=direction:active\r\n";
    }
    useAudio = true;
    let senderProtocol = "RTP/AVPF";
    let sdpCryptoAttr = "";
    if(useSrtp){
        useAudio = false;
        senderProtocol = "RTP/SAVPF";

        sdpCryptoAttr = "a=crypto:2 AES_CM_128_HMAC_SHA1_80 inline:QUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVoxMjM0|2^31|1:1\r\n"; 
    }

    let rtpSdpOffer = 
        "v=0\r\n"
        + "o=- 0 0 IN IP4 " + senderIp + "\r\n"
        + "s=Kurento Tutorial - RTP Receiver\r\n"
        + "c=IN IP4 " + senderIp + "\r\n"
        + "t=0 0\r\n";
    
        if(useAudio){
            rtpSdpOffer +=
            "m=audio " + senderRtpPortAudio + " RTP/AVPF 96\r\n"
            + "a=rtpmap:96 opus/48000/2\r\n"
            + "a=sendonly\r\n"
            + sdpComediaAttr
            + "a=ssrc:" + senderSsrcAudio + " cname:" + senderCname + "\r\n";
        }
        rtpSdpOffer +=
        "m=video " + senderRtpPortVideo + " " + senderProtocol + " 103\r\n"
        + sdpCryptoAttr
        + "a=rtpmap:103 " + senderCodecV + "/90000\r\n"
        + "a=rtcp-fb:103 goog-remb\r\n"
        + "a=sendonly\r\n"
        + sdpComediaAttr
        + "a=ssrc:" + senderSsrcVideo + " cname:" + senderCname + "\r\n"
        + "";
    return rtpSdpOffer;
}

async function makeRtpEndpoint(pipeline, useSrtp = false) {
    let rtpEndpoint;
    if(!useSrtp){
        rtpEndpoint = await pipeline.create('RtpEndpoint');
        return rtpEndpoint;
    }
    let cryptoSuite = kurento.getComplexType('CryptoSuite')('AES_128_CM_HMAC_SHA1_80');
    let srtpMasterKey = "4321ZYXWVUTSRQPONMLKJIHGFEDCBA";
    console.log(`Getting Cypto Suite : ${cryptoSuite}`);
    let sdesDict = {
        key : srtpMasterKey,
        crypto : cryptoSuite
    };
    rtpCrypto = kurento.getComplexType('SDES')(sdesDict);
    rtpEndpoint = await pipeline.create('RtpEndpoint', {crypto : rtpCrypto});
    return rtpEndpoint;
}

async function startRtpEndpoint(ws, sessionId, rtpEp, useComedia = false, useAudio = false, useSrtp = false){
    await addBaseEventListeners(sessionId, rtpEp, 'RtpEndpoint');
    let sdpOffer = rtpEndpointConfig(useComedia, useAudio, useSrtp);
    console.log(`From startRtpEndpoint Fake SDP offer from App to KMS ${sdpOffer}`);
    let sdpAnswer = await rtpEp.processOffer(sdpOffer);
    console.log(`From startRtpEndpoint SDP answer from KMS to App ${sdpAnswer}`);
}


app.use(express.static(path.join(__dirname, 'client')));
