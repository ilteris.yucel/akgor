const getProtocol = (link) => {
    let splitedLink = link.split('/');
    return splitedLink[0].replace(':', '');
}

const detectProtocol = (link, protocol) => {
    const splitedLink = link.split('/');
    const detectedProtocol = splitedLink[0].replace(':', '');
    return detectedProtocol === protocol;
}



export {getProtocol, detectProtocol};