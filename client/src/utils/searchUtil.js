const searchOneTag = (array, input, tag ='name') => {
    array = array.filter((val) => {
        return val[tag].includes(input);
    })
    return array;
}

const searchMultiTag = (array, input, tagList = ['name', 'link', 'type']) => {
    array = array.filter((val) => {
        let ret = [];
        tagList.forEach((tag)=> {
            ret.push(val[tag].includes(input));
        });
        return ret.some((val) => {return val});
    });
    return array;
}

export {searchOneTag, searchMultiTag};