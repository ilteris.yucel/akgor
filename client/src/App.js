import './App.css';
import 'blueprint-css/dist/blueprint.css';
import "../node_modules/normalize.css";
import React from 'react';
import { HashRouter, Route, Switch} from 'react-router-dom';
import Header from './components/header';
import StreamsPage from './components/streamsPage';
import HomePage from './components/homePage';
import ProfilePage from './components/profilePage';
import MainMenu from './components/mainMenu';
import VideoPage from './components/videoPage'

const App = (props) => {
  return (
    <HashRouter>
      <div className="App">
        <div bp="grid">
          <div bp="12">
            <Header></Header>
          </div>
          <div bp="3" className="left-align-item">
            <MainMenu></MainMenu>
          </div>
          <div id="page" bp="9">
            {/*
              exact path yazınca sayfaların iç içe geçmesi sorunu çözüldü
              ne ayak onu anla
             */}
            <Route exact path="/" component={HomePage}></Route>
            <Route path="/streams" component={StreamsPage}></Route>
            <Route path="/profile" component={ProfilePage}></Route>
            <Switch>
              <Route path="/video/:id" children={<VideoPage/>}/>
            </Switch>
          </div>
        </div>
        <div id="footer">&#169; 2021 PiriReis Bilişim Teknolojileri</div>
      </div>
    </HashRouter>

  );

}

export default App;
