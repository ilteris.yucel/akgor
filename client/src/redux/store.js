import { createStore } from 'redux';
import reducer from './reducers/streamInfo.js';
import { getStreamInfo } from '../httpsConnection';

export const configStoreAsync = () => {
    return new Promise((resolve, reject) => {
        let initialState = {
            streamInfoJSON: null,
            jsonLength: 0
        };
        try{
            getStreamInfo().then(streamInfo => {
                initialState.streamInfoJSON = streamInfo;
                initialState.jsonLength = Object.keys(initialState.streamInfoJSON).length;
                const store = createStore(reducer, initialState);
                resolve(store);
            })
        }catch(err){    
            const store = createStore(reducer, initialState);
            console.log(store.getState());
            resolve(store);
        }
    })
} 