const getStreams = () => ({
    type : 'GET_STREAM'
});

const addStream = (stream) => ({
    type: 'ADD_STREAM',
    payload: stream
});

const removeStream = (id) => ({
    type: 'REMOVE_STREAM',
    payload: {id: id}
});

const updateStream = (id, stream) => ({
    type: 'UPDATE_A_STREAM',
    payload: {
        id: id,
        stream: stream
    }
});
const searchStream = (input, tags) => ({
    type: 'STREAM_SEARCH',
    payload: {
        input: input,
        tags: tags
    }
})

export {getStreams, addStream, removeStream, updateStream, searchStream};
