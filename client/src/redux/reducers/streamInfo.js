import { searchOneTag, searchMultiTag } from "../../utils/searchUtil";
const initialState = {
    streamInfoJSON : null,
    jsonLength : 0
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_STREAM':
            return state;
        case 'ADD_STREAM':
            let addedArr = [...state.streamInfoJSON, action.payload];
            localStorage.setItem('initialState', JSON.stringify(addedArr));
            localStorage.setItem('isChanged', JSON.stringify(true));
            return {
                ...state,
                streamInfoJSON: addedArr,
                jsonLength: state.jsonLength += 1
            }
        case 'REMOVE_STREAM':
            let removedArr = state.streamInfoJSON.filter((val, ind, arr) => {return ind !== action.payload.id});
            localStorage.setItem('initialState', JSON.stringify(removedArr));
            localStorage.setItem('isChanged', JSON.stringify(true));
            return {
                ...state,
                streamInfoJSON: removedArr,
                jsonLength: state.jsonLength -= 1
            }
        case 'UPDATE_A_STREAM':
            //TODO Daha az amelece array güncellemesi yapılacak.
            let updatedArr = [...state.streamInfoJSON.slice(0, action.payload.id),
                action.payload.stream,
                ...state.streamInfoJSON.slice(action.payload.id+1)
            ];
            localStorage.setItem('initialState', JSON.stringify(updatedArr));
            localStorage.setItem('isChanged', JSON.stringify(true));
            return {
                ...state,
                streamInfoJSON: updatedArr
            }
            case 'STREAM_SEARCH':
                let searchResult = JSON.parse(localStorage.getItem('initialState'));
                if(action.payload.input){
                    searchResult = typeof action.payload.tags === "string" ? 
                        searchOneTag(searchResult, action.payload.input, action.payload.tags) :
                        searchMultiTag(searchResult, action.payload.input, action.payload.tags); 
                }
                console.log("After Search");
                console.log(searchResult);
                return{
                    ...state,
                    streamInfoJSON: searchResult,
                    jsonLength: searchResult.length,
                }
        default:
            return state;
    }
}
    
export default reducer;

