import React from 'react';
import { InputGroup, Tag} from "@blueprintjs/core";
import {searchStream} from "../redux/action";
import { connect } from 'react-redux';

class UtilBar extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.handleTag = this.handleTags.bind(this);
        this.state ={
            filterValue: "",
            tags:['name'],
            tagsStatus:{
                'name': true,
                'link': false,
                'type': false,
            }
        }
    }
    componentDidMount() {

    }
    handleInput(val){
        this.setState({filterValue : val});
        if(this.state.tags.length === 0){
            this.props.search(val, 'name');
        }else if(this.state.tags.length === 1){
            this.props.search(val, this.state.tags[0]);
        }else{
            this.props.search(val, this.state.tags);
        }
            
    }
    async handleTags(tag){
        let span = document.getElementById(tag);
        //TODO deep copy trick, lodash ile değiştirilecek
        let statObj = JSON.parse(JSON.stringify(this.state)).tagsStatus;
        let tags = JSON.parse(JSON.stringify(this.state)).tags;

        let way = statObj.tag ? 'TF' : 'FT';
        statObj.tag = way === 'TF' ? false : true;
        if(way === 'TF'){
            tags = tags.filter((t) => {return t !== tag});
            span.style.backgroundColor ='black';
        }else{
            span.style.backgroundColor ='red';
            tags.push(tag);
        }
        await this.setState({tags: tags});
        await this.setState({tagsStatus:statObj});
        console.log(this.state.tags);       
    }
    render() {
        //const maybeSpinner = this.state.filterValue ? <Spinner size={IconSize.STANDARD} /> : undefined;
        return(
            <InputGroup
                leftIcon='filter'
                placeholder="Yayınları Filtrele"
                value={this.state.filterValue}
                onChange={e => this.handleInput(e.target.value)}
                rightElement={
                    <div>
                        <Tag
                         id={'name'}
                         interactive={true}
                         onClick={()=>this.handleTags('name')}
                         style={{backgroundColor:'red'}}
                        >YAYIN ADI</Tag>
                        <Tag
                        id={'link'}
                        interactive={true}
                        onClick={()=>this.handleTags('link')}
                        style={{backgroundColor:'black'}}
                        >YAYIN LINKI</Tag>
                        <Tag
                        id={'type'}
                        interactive={true}
                        onClick={()=>this.handleTags('type')}
                        style={{backgroundColor:'black'}}
                        >YAYIN TİPİ</Tag>
                    </div>

                }
            />
        )
    }
}

const mapDispatchToProps = dispatch => ({
    search : (input, tags) => dispatch(searchStream(input, tags))
})

export default connect(null, mapDispatchToProps)(UtilBar);