import { Alignment, AnchorButton, ButtonGroup} from "@blueprintjs/core";
import React from 'react';
import { NavLink } from 'react-router-dom';

class MainMenu extends React.Component{
    constructor(props){
      super(props);
      this.state = { 
        alignText : Alignment.LEFT,
        vertical : true,
        large: true
      };
    }
    componentDidMount(){
    }
    render(){
      const menuStyle = {
        marginTop : 16
      };
      return(
        <ButtonGroup style={menuStyle} {...this.state}>
          <AnchorButton icon="home"><NavLink to="/">ANASAYFA</NavLink></AnchorButton>
          <AnchorButton icon="mobile-video"><NavLink to="/streams">YAYINLAR</NavLink></AnchorButton>
          <AnchorButton icon="user"><NavLink to="/profile">PROFİL</NavLink></AnchorButton>
        </ButtonGroup>
      )
    }
  }

  export default MainMenu;