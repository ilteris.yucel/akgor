import 'blueprint-css/dist/blueprint.css';
import "../../node_modules/normalize.css";
import "../../node_modules/@blueprintjs/core/lib/css/blueprint.css";
import "../../node_modules/@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import {React, useEffect} from 'react';
import { writeStreamInfo } from '../httpsConnection';
import StreamTable from './streamTable';
import UtilBar from './utilBar';
import AddButton from './addButton';

const StreamsPage = (props) => {

  useEffect(() => {
    const interval = setInterval(() => {
      const status = JSON.parse(localStorage.getItem('isChanged'));
      if(status){
        const streamInfo = JSON.parse(localStorage.getItem('initialState'));
        writeStreamInfo(streamInfo)
          .then((result) => console.log(result))
          .catch((error) => console.log(`${error} while post request`))
          .finally(() => localStorage.setItem('isChanged', JSON.stringify(false)));
      }else{
        console.log('Initial state does not changed!');
      }
    }, 1000)
    return () => {
      clearInterval(interval);
    }
  }, []);
  return (
    <div className="StreamsPage">
        <div bp="grid">
            <div bp="8"><UtilBar></UtilBar></div>
            <div bp="2"></div>
            <div bp="2"><AddButton></AddButton></div>

            <StreamTable></StreamTable>
        </div>
    </div>
  );
}

export default StreamsPage;