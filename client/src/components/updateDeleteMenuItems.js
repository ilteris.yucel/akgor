import React from 'react';
import {Menu, MenuItem, MenuDivider} from "@blueprintjs/core";
import { Popover2 } from "@blueprintjs/popover2";
import {removeStream} from "../redux/action";
import { connect } from 'react-redux';
import AddButtonContext from './addButtonContext';

class UpdateDeleteMenuItems extends React.Component {
    constructor(props) {
        super(props);
        this.deleteHandler = this.deleteHandler.bind(this);
        this.state = {}
    }

    componentDidMount() {

    }
    deleteHandler(){
        console.log(this.props);
        this.props.delete(this.props.streamId);
    }
    render(){
        return(
            <Menu>
                <Popover2
                    content={<AddButtonContext task={"update"} streamId={this.props.streamId}></AddButtonContext>}
                    fill={true}
                >
                    <MenuItem shouldDismissPopover={false} icon="cog" text="Yayını Güncelle"></MenuItem>
                </Popover2>
                <MenuDivider></MenuDivider>
                <MenuItem icon="delete" text="Yayını Sil" onClick={this.deleteHandler}></MenuItem>
            </Menu>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    delete : (id) => dispatch(removeStream(id))
});

export default connect(null, mapDispatchToProps)(UpdateDeleteMenuItems);