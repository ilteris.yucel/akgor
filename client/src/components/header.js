import { Alignment, Button, Navbar} from "@blueprintjs/core";
import React from 'react';

class Header extends React.Component {
    constructor(props) {
      super(props);
      this.state = {};
    };
  
    componentDidMount() {
  
    }
  
    render(){
      return(
        <Navbar>
          <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>PİRİ REİS</Navbar.Heading>
            <Navbar.Divider/>
          </Navbar.Group>
          <Navbar.Group align={Alignment.RIGHT}>
            <Button icon="person">YÖNETİCİ PANELİ</Button>
            <Button icon="log-out">ÇIKIŞ YAP</Button>
          </Navbar.Group>
        </Navbar>
      )
    }
  
  }

  export default Header;