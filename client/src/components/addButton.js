import React from 'react';
import {Button} from "@blueprintjs/core";
import { Popover2 } from "@blueprintjs/popover2";
import AddButtonContext from "./addButtonContext";
import '../css/addButton.css';


class AddButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {

    }

    render(){
        return(
            <Popover2
                fill={true}
                className={"right-margin-button"}
               content={<AddButtonContext></AddButtonContext>} 
            >
                <Button
                    icon={'add'}
                    text={'YAYIN EKLE'}
                    fill={true}
                >
                </Button>
            </Popover2>

        )
    }
}

export default AddButton;