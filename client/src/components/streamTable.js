import UpdateDeleteMenu from './updateDeleteMenu';
import React from 'react';
import { connect } from 'react-redux';

class StreamTable extends React.Component {
    constructor(props) {
      super(props);
      this.createTBody = this.createTBody.bind(this);
    }
    componentDidMount() {

    }
    handleClick = () => {
      console.log("Click");
    }
    createTBody(){
      console.log(this.props.streamJson);
      let bodyArr = [];
      for(let i=0; i < this.props.jsonLength; i++){
        let comp = <UpdateDeleteMenu key={`ud-${i}`} 
          streamKey={`sc-${i}`}
          streamId={i} 
          streamName={this.props.streamJson[i]['name']}
          streamLink={this.props.streamJson[i]['link']}
          streamType={this.props.streamJson[i]['type']}></UpdateDeleteMenu>;
          bodyArr.push(comp);
      }
      return bodyArr;
    }
    render(){
      return(
        <div bp="grid 12">
          <div bp="grid">
            {this.createTBody()}
          </div>
        </div>
      );
    };
  
  }
  const mapStateToProps = state => ({
    streamJson: state.streamInfoJSON,
    jsonLength: state.jsonLength
  });
  export default connect(mapStateToProps)(StreamTable);