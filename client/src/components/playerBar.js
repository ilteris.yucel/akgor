import React from 'react';
class PlayerBar extends React.Component {
    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
        this.state = {

        }
    }
    componentDidMount() {
        const dur = this.props.width / (this.props.dur / this.props.now);
        const canvas = this.canvasRef.current;
        const ctx = canvas.getContext("2d");
        ctx.fillStyle = '#F6F9FB';
        ctx.fillRect(0, 0, this.props.width, this.props.height);
        ctx.fillStyle = '#000000';
        ctx.fillRect(0, 0, dur, this.props.height);
    }
    render(){

        return <canvas ref={this.canvasRef}></canvas>
    }
}

export default PlayerBar;