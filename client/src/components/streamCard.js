import { AnchorButton, Card, Elevation, H5, Icon} from "@blueprintjs/core";
import React from 'react';
import { NavLink } from 'react-router-dom';
import '../css/streamCard.css';

class StreamCard extends React.Component {
    render(){
      const cardStyle = {
        marginTop: 16,
        marginRight: 16,
        textAlign: 'left',
      }
      return(

          <Card style={cardStyle} interactive={true} elevation={Elevation.TWO}>
            <H5>{this.props.name}</H5>
            <Icon icon="link" color="blue"></Icon><a href="#">&nbsp;&nbsp;&nbsp;{this.props.link}</a>
            <p>{this.props.description}</p>
            <p><Icon icon="application" color="blue"></Icon>&nbsp;&nbsp;&nbsp;{this.props.type}</p>
            <AnchorButton rightIcon="caret-right"><NavLink to={`/video/${this.props.streamId}`}>YAYINI BAŞLAT</NavLink></AnchorButton>
          </Card>

  
      )
    }
}

export default StreamCard;