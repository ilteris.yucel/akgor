import React from 'react';
import {Button, InputGroup, H5} from "@blueprintjs/core";
import { Classes } from "@blueprintjs/popover2";
import "../css/addButton.css";
import {addStream} from "../redux/action";
import {updateStream} from "../redux/action";
import { connect } from 'react-redux';
import {getProtocol} from '../utils/baseUtils';

class AddButtonContext extends React.Component {
    constructor(props) {
        super(props);
        this.handleNameInput = this.handleNameInput.bind(this);
        this.handleLinkInput = this.handleLinkInput.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.state = {
            nameInput: "",
            linkInput: ""
        };
    }
    componentDidMount() {

    }
    handleNameInput(val){
        this.setState({nameInput: val});
    }
    handleLinkInput(val){
        this.setState({linkInput: val});
    }
    handleButtonClick(){
        console.log("this : " + this);
        console.log("this.state:" + this.state);
        console.log("linkİnput : " + this.state.linkInput);
        console.log("nameİnput : " + this.state.nameInput);
        const stream = {
            name: this.state.nameInput,
            link: this.state.linkInput,
            type: getProtocol(this.state.linkInput)
        }
        //TODO Daha az amelece UPDATE ADD ayrımı yapılacak
        if(this.props.task === "update"){
            console.log("UPDATE");
            this.props.update(this.props.streamId, stream);
        }else{
            this.props.add(stream);
        }
        
    }
    render(){
        return(
            <div id="container-div">
                <H5 id="header">YAYIN EKLEME PENCERESİ</H5>
                <InputGroup
                    id="inputs"
                    leftIcon='new-text-box'
                    placeholder="Yayın Adını Giriniz:"
                    value={this.state.nameInput}
                    onChange={e => this.handleNameInput(e.target.value)}
                >
                
                </InputGroup>
                <InputGroup
                    id="inputs"
                    leftIcon='link'
                    placeholder="Yayın Linkini Giriniz:"
                    value={this.state.linkInput}
                    onChange={e => this.handleLinkInput(e.target.value)}
                >
                
                </InputGroup>
                <div bp="grid 4">
                    <div>
                        <Button
                            className={Classes.POPOVER2_DISMISS}
                            icon='tick-circle'
                            text="KAYDET"
                            fill={true}
                            onClick={this.handleButtonClick}
                        >

                        </Button>
                    </div>
                    <div>

                    </div>
                    <div>
                    <Button
                            className={Classes.POPOVER2_DISMISS}
                            intent={'danger'}
                            icon='key-backspace'
                            text="GERİ"
                            fill={true}
                        >

                        </Button>
                    </div>
                </div>
            </div>

        )
    }
}


const mapDispatchToProps = dispatch => ({
    add : (stream) => dispatch(addStream(stream)),
    update :(id, stream) => dispatch(updateStream(id, stream)),
});

export default connect(null, mapDispatchToProps)(AddButtonContext);