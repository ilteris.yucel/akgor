import React from 'react';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import  Player  from './player';
import '../css/videoPage.css';

class VideoPage extends React.Component {
    constructor(props) {
        super(props);
        this.vw = React.createRef();
        this.state = {
            id: null,
            link: '',
            name: '',
            type: '',
            wslink: 'wss://localhost:8443/media-server',
            vidWidth: 0,
        };
    }
    componentDidMount() {
        const id = this.props.match.params.id;
        const name = this.props.streamJson[id]['name'];
        const link = this.props.streamJson[id]['link'];
        const type = this.props.streamJson[id]['type'];

        this.setState({id:id, link:link, name:name, type:type});
        
        const vw = 3 * (window.innerWidth / 4)-32;
        this.setState({vidWidth:vw});
    }
    componentWillUnmount(){
        console.log("Unmount video page...");
        //this.state.rtspStream.stop();
    }
    render() {
        return(
            <div bp='grid 12'>
                <div bp="grid">
                   <div bp='3'>{this.state.name}</div>
                   <div bp='6'>{this.state.link}</div>
                   <div bp='3' >{this.state.type}</div>
                   <div bp='12' ref={this.vw} className='left-align-item'>
                       <Player wslink={this.state.wslink} link={this.state.link} containerWidth={this.state.vidWidth}></Player>
                   </div>
                   <div bp="grid 4">

                   </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    streamJson: state.streamInfoJSON
  });
export default withRouter(connect(mapStateToProps)(VideoPage));