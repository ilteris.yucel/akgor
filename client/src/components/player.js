import React from 'react';
import { Button, ButtonGroup, Spinner, Slider } from '@blueprintjs/core';
import { Popover2 } from '@blueprintjs/popover2';
import { RTSPStream } from '../rtspStream';
import '../css/player.css';

class Player extends React.Component {
    constructor(props) {
        super(props);
        this.video = React.createRef();
        this.container = React.createRef();
        this.renderTimeout = 0;
        this.handlePlay = this.handlePlay.bind(this);
        this.handlePause = this.handlePause.bind(this);
        this.handleResolution = this.handleResolution.bind(this);
        this.waitMedia = this.waitMedia.bind(this);
        this.getMedia = this.getMedia.bind(this);
        this.state = {
            status: 'playing',
            wait: true,
            resolution : 1,
            resolutionList : ['480', '720', '1080'],
            rtspStream: null,
            streams : {
                '480': null,
                '720': null,
                '1080': null
            },
            wf :{
                '480': 0.5,
                '720': 0.6667,
                '1080': 1
            }
        };
    }
    componentDidMount() {
        const wslink = this.props.wslink;
        const link = "rtsp://localhost:8554/test"; //this.props.link;
        this.getMedia(wslink, link);

    }
    componentWillUnmount() {
        this.state.rtspStream.stop();
        clearTimeout(this.renderTimeout);
    }
    getMedia(wslink, link){
        const rtspStream = new RTSPStream(wslink, link, null);
        const resolution = this.state.resolutionList[this.state.resolution];
        this.waitMedia(rtspStream, function(){
            this.setState({rtspStream: rtspStream});
            this.setState({streams: rtspStream.medias});
            console.log(rtspStream);
            this.setState({wait: false});
            this.video.current.width = this.props.containerWidth * this.state.wf[resolution];
            this.video.current.height = this.video.current.width * 0.5625;
            this.video.current.srcObject = rtspStream.medias[resolution];
            const playPromise = this.video.current.play();
            if(playPromise !== undefined) {
                playPromise.then(() => {
                    this.container.current.style.backgroundColor = 'black';
                }).catch(function(error) {
                    console.error(error);
                });
            }
        }.bind(this));

    }
    waitMedia(stream, callback) {
        this.renderTimeout = setTimeout(() => {
            const media = stream.getMedia();
            const flowIn = stream.mediaFlowIn;
            if(media && flowIn){
                console.log('Media is ready');
                if(callback !== null) callback();
            }else{
                console.log("Media is waiting...");
                this.waitMedia(stream, callback);
            }
        }, 5)
    }
    handlePlay(){
        this.setState({status:'playing'});
        this.state.rtspStream.resume();
        this.video.current.play();
        console.log(this.video.current.currentTime);
    }
    handlePause(){
        this.setState({status:'paused'});
        this.state.rtspStream.pause();
        this.video.current.pause();
        console.log(this.video.current.currentTime);
    }
    handleResolution(event){
        const resolution = event.currentTarget.value;
        console.log("Change to " + resolution);
        const resoId = this.state.resolutionList.indexOf(resolution);
        this.setState({resolution:resoId});
        const stream = this.state.streams[resolution];
        console.log(stream);
        this.video.current.pause();
        this.video.current.width = this.props.containerWidth * this.state.wf[resolution];
        this.video.current.height = this.video.current.width * 0.5625;
        this.video.current.srcObject = stream;
        this.video.current.play();
    }

    render(){
        const status = this.state.status;
        const resolution = this.state.resolution;
        const resolutionList = this.state.resolutionList;        
        const containerWidth = this.props.containerWidth;
        const width = containerWidth;
        const height = width * 0.5625;
        const videoDivStyle = {
            width: width,
            height: height,
            position: "relative"
        };
        const buttonStyle = {
            width: 40,
            height: 40,
            float: 'left'
        }
        const buttonGroupStyle = {
            width: 80,
            height: 40,
            float: 'left'
        }
        const sliderStyle = {
            width: width-120,
            paddingLeft: 16,
            paddingRight: 16,
            height: 40,
            float: 'left'
        }
        let button, button1, buttonGroup; 
        let buttonList = [];
        button = status === 'paused' ? button = <Button fill={true} icon="play" onClick={this.handlePlay}></Button> : 
            <Button fill={true} icon="pause" onClick={this.handlePause}></Button>;
        button1 = <Button fill={true}>{resolutionList[resolution]}</Button>;
        resolutionList.forEach((resolution) => {
            const button = <Button value={resolution} onClick={this.handleResolution}>{resolution}</Button>;
            buttonList.push(button);
        })
        buttonGroup = 
            <ButtonGroup vertical={true}>
                {buttonList}
            </ButtonGroup>;
        return(
            <div bp="12" className="parent">
                    <div ref={this.container} style={videoDivStyle}>
                        {
                            this.state.wait === false ? <video className="vid-content" width={width} height={height} ref={this.video} muted={true}></video> :
                            <Spinner className="vid-content"></Spinner>
                        }                       
                    </div>
                    <div>
                        <div style={buttonStyle}>
                            {button}
                        </div>
                        <div style={sliderStyle}>
                            <Slider 
                                min={0}
                            ></Slider>
                        </div>
                        <div style={buttonGroupStyle}>
                            <ButtonGroup>
                                <Button icon="record"></Button>
                                <Popover2
                                    fill={true}
                                    content={buttonGroup}
                                    placement={'top-start'}
                                >
                                    {button1}
                                </Popover2>
                            </ButtonGroup>
                        </div>
                    </div>
            </div>
        )
    }
}

export default Player;