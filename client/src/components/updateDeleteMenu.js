import React from 'react';
import { Popover2 } from "@blueprintjs/popover2";
import StreamCard from './streamCard';
import UpdateDeleteMenuItems from './updateDeleteMenuItems';

class UpdateDeleteMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {

    }

    render() {
        return(
            <div bp="4@lg 6@md 12@sm">
                <Popover2
                    content={<UpdateDeleteMenuItems streamId={this.props.streamId}></UpdateDeleteMenuItems>}
                    fill={true}
                    enforceFocus={false}
                    placement={"bottom-end"}
                >
                    <StreamCard key={this.props.streamKey}
                        streamId={this.props.streamId}
                        name={this.props.streamName}
                        link={this.props.streamLink}
                        type={this.props.streamType}>
                    </StreamCard>
                </Popover2>
            </div>
        )
    }
}

export default UpdateDeleteMenu;