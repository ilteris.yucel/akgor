import PlayerWebRtcPeer from './playerWebRtcPeer';

class RTSPStream {
    constructor(url, uri, videoTag){
        this.url = url;
        this.ws = new WebSocket(url);
        this.uri = uri;
        this.configureWS();
        this.mediaElements = {
            remoteVideo: videoTag,
            localVideo: null
        }
        this.medias = {
            '480': null,
            '720': null,
            '1080': null
        }
        this.mediaFlowIn = false;

        this.webRtcConnection = new WebRtcConnection(this.ws, this.uri, this.mediaElements);
        this.webRtcConnection.configureOptions(); 
    }
    getMedia(){
        let ret = true;
        Object.keys(this.medias).forEach(key => {
            if(!this.medias[key])
                ret = false;
        })
        return ret;
    }
    configureWS(){
        this.ws.onopen = async () => {
            await this.webRtcConnection.start();
        }
        this.ws.onmessage = async (_message) => {
            const message = JSON.parse(_message.data);
            const messageId = message.id;
            const sdpAnswer = message.sdpAnswer;
            const resolution = message.resolution;
    
            switch(messageId){
                case 'PROCESS_SDP_ANSWER_PLAYER_EP':
                    await this.start(sdpAnswer, resolution);
                    break;
                case 'STOP':
                    console.log('STOPPING');
                    this.stop();
                    break; 
                case 'ERROR':
                    console.log(message.desc);
                    break;
                case 'ADD_ICE_CANDIDATE':
                    await this.handleIceCandidate(message);
                    break;
                case 'INFO':
                    console.log(message.info);
                    break;
                case 'VIDEO_INFO':
                    console.log(message);
                    break;
                case 'STATE_CHANGE':
                    console.log("STATE_CHANGE ");
                    this.webRtcConnection.kmsState = true;
                    console.log(this.webRtcConnection.kmsState);
                    break;
                case 'MEDIA_FLOW_IN':
                    console.log('Media flowing...');
                    this.mediaFlowIn = true;
                    break;
                case 'MEDIA_FLOW_OUT':
                    console.log('Media not flowing...');
                    this.mediaFlowIn = false;
                    break;
                default:
                    console.log('Something went wrong');
                    break;  
            }
        }        
    }
    async start(sdpAnswer, resolution){
        console.log('SDP answer received from server. Processing ...');
        //console.log(sdpAnswer);
        await this.webRtcConnection.peers[resolution].processAnswer(sdpAnswer);
        this.medias[resolution] = this.webRtcConnection.peers[resolution].media;
    }
    async handleIceCandidate(message) {
        await this.webRtcConnection.peers[message.resolution].addIceCandidate(message.candidate);  
    }
    stop(){
        let message = {
            id : 'STOP'
        };
        this.webRtcConnection.sendMessage(message);
        ['480', '720', '1080'].forEach(function(reso){
            const peer = this.webRtcConnection.peers[reso];
            if(peer){
                peer.dispose();
                this.webRtcConnection.peers[reso] = null;
            }   
        }.bind(this));
        const timeout = setTimeout(function(){
            console.log("WS closing");
            this.ws.close();
            clearTimeout(timeout);
            clearTimeout(this.webRtcConnection.wsTimeout);
        }.bind(this), 100);
     
    }
    pause(){
        const message = {
            'id' : 'PAUSE'
        };
        this.webRtcConnection.sendMessage(message);
    }
    resume(){
        const message = {
            'id': 'RESUME'
        }
        this.webRtcConnection.sendMessage(message);
    }
}

class WebRtcConnection{
    constructor(ws, uri, mediaElements, mediaConstraints){
        this.ws = ws;
        this.uri = uri;
        this.mediaElements = mediaElements;
        this.wsTimeout = null;
        this.mediaConstraints = mediaConstraints ? mediaConstraints : {
            audio: false,
            video: true
        }
        this.options = null;
        this.webRtcPeer = null;
        this.peers = {
            '480': null,
            '720': null,
            '1080': null
        };
        this.kmsState = true;
    }
    configureOptions(){
        this.options = {
            remoteVideo: this.mediaElements.remoteVideo,
            localVideo: this.mediaElements.localVideo,
            mediaConstraints: this.mediaConstraints,
            ws: this.ws
        }
    }
    updateOptions(attr, val){
        this.options[attr] = val;
    }
    /*onIceCandidate(candidate){
        console.log('Local candidate' + JSON.stringify(candidate));
        console.log(this);
        let message = {
           id : 'ADD_ICE_CANDIDATE',
           candidate : candidate
        };
        this.sendMessage(message);
    }*/
    sendMessage(_message){
        let message = JSON.stringify(_message);
        this.waitSocketConnection(this.ws, () => {
            this.ws.send(message);
        });
    }
    waitSocketConnection(socket, callback){
        this.wsTimeout = setTimeout(() => {
            if(socket.readyState === 1){
                console.log('WebSocket connection established');
                if(callback !== null) callback();
            }else{
                console.log('WebSocket connection is waiting...');
                this.waitSocketConnection(socket, callback);
            }
        }, 5)
    }
    async start(){
        //this.webRtcPeer = new PlayerWebRtcPeer(this.options);
        //await this.webRtcPeer.createPeer();
        //const sdpOffer = await this.webRtcPeer.createOffer();;
        /*let message = {
            id: 'PROCESS_SDP_OFFER_PLAYER_EP',
            uri: this.uri,
            sdpOffer: sdpOffer
        }
        this.sendMessage(message);*/
        //let timeout, timeout1, timeout2;
        //this.waitKms(timeout, '480');
        //this.waitKms(timeout1, '720');
        //this.waitKms(timeout2, '1080');
        this.waitKms(0);

    }
    waitKms(index){
        console.log("İndex is " + index);
        const resoList = ['480', '720', '1080'];
        this.timeout = setTimeout(async () => {
            if(index === 2 && this.kmsState){
                await this.createPeer(resoList[index]);
                clearTimeout(this.timeout);
                console.log('Peer Creation Finished');
                return;
            }else if(index !== 2 && this.kmsState){
                this.kmsState = false;
                await this.createPeer(resoList[index]);
                console.log('Peer ' + index + " creating...")
                index += 1;
                this.waitKms(index);
            }else{
                console.log('KMS is waiting...');
                this.waitKms(index);
            }
        }, 100);
    }
    async createPeer(resolution){
        this.peers[resolution] = new PlayerWebRtcPeer(this.options);
        await this.peers[resolution].createPeer(resolution);
        const sdpOffer = await this.peers[resolution].createOffer();
        const message = {
            id: 'PROCESS_SDP_OFFER_PLAYER_EP',
            uri: this.uri,
            sdpOffer: sdpOffer,
            resolution: resolution,
            filename: 'test',
            firstflag: resolution === '480' ? true : false
        }
        this.sendMessage(message);
    }
}

export { RTSPStream };
