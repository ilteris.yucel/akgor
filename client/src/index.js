import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {configStoreAsync} from './redux/store.js'
import { Provider } from 'react-redux'
import reportWebVitals from './reportWebVitals';


configStoreAsync().then(result => {
  const store = result;
  //TODO Search Bar için ilk durumu localStorage ekledik.
  //Daha az amelece çözüm bulunacak.
  localStorage.setItem('initialState', JSON.stringify(store.getState().streamInfoJSON));
  localStorage.setItem('isChanged', JSON.stringify(false));
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App postInterval={0} />
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  
  );
})


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
