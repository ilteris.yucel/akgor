const https = require('https');

const getStreamInfoOptinos = {
    host: 'localhost',
    port: '8443',
    path: '/get-json',
    method: 'GET'
}

const postStreamInfoOptinos = {
    host: 'localhost',
    port: '8443',
    path: '/post-json',
    method: 'POST',
    headers: { 'Content-Type': 'application/json',
                'Content-Length': null
    }
}

const getWSConnectionOptinos = {
    host: 'localhost',
    port: '8443',
    path: '/get-ws-connection',
    method: 'GET'
}

const getWSConnection = () => {
    let wsConnection = null;
    return new Promise((resolve, reject) => {
        let req = https.request(getWSConnectionOptinos, (res) => {
            let data = '';
            if(res.statusCode !== 200){
                reject(`Invalid Status Code: ${res.statusCode}`);
            }
            res.on('data', (d) => {
                data += d
            });
            res.on('end', () => {
                wsConnection = data;
                resolve(wsConnection);
            });
        });
        req.on('error', (err) => {
            reject(err);
        });
        req.end();
    })
}

const getStreamInfo = () => {
    let streamInfoJSON = null;
    return new Promise((resolve, reject) =>{
        let req = https.request(getStreamInfoOptinos, (res) => {
            let data = '';
            if(res.statusCode !== 200){
                reject(`Invalid Status Code: ${res.statusCode}`);
            }
            res.on('data', (d) =>{
                data += d;
            });
            res.on('end', () =>{
                streamInfoJSON = JSON.parse(data);
                resolve(streamInfoJSON);
            })
        })
        req.on('error', (err) => {
            reject(err);
        })
        req.end();
    })
}
const writeStreamInfo = (streamInfo) => {
    postStreamInfoOptinos.headers['Content-Length'] = streamInfo.length;
    return new Promise((resolve, reject) => {
        let req = https.request(postStreamInfoOptinos, (res) => {
            if(res.statusCode !== 200){
                reject(`Invalid Status Code: ${res.statusCode}`);
            }
            res.on('data', (data) => {
                //process.stdout.write(data);
                resolve(data);
            });
        });

        req.on('error', (error) => {
            reject(error)
        });
        const data = {
            streamInfoJSON: streamInfo
        }
        req.write(JSON.stringify(data));
        req.end();
    });
}
export {getStreamInfo, writeStreamInfo, getWSConnection};