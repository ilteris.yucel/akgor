const freeice = require('freeice');

export default class PlayerWebRtcPeer{
    constructor(options){
        this.options = options;
        this.remoteVideo = this.options.remoteVideo;
        this.ws = this.options.ws;
        this.configuration = {'iceServers': freeice()};
        this.peer = null;
        this.media = null;
        this.resolution = null;

    }
    async createPeer(resolution){
        this.peer = new RTCPeerConnection(this.configuration);
        this.resolution = resolution;
        const ws = this.ws;
        this.peer.addTransceiver('audio', {
            direction: 'recvonly'
        });
        this.peer.addTransceiver('video', {
            direction: 'recvonly'
        });
        this.peer.onopen = () => console.log('WebRTC Communication Established');
        this.peer.onerror = (err) => console.log(`Error while WebRTC Communication ${err}`);
        this.peer.onicecandidate = (event) => {
            const candidate = event.candidate;
            if(candidate){
                const message = {
                    id : 'ADD_ICE_CANDIDATE',
                    candidate : event.candidate,
                    resolution: this.resolution,
                 };
                 ws.send(JSON.stringify(message));
            }
        }
        this.peer.onaddstream = function(event){
            const rawStream = event.stream;
            const hintedStream = rawStream.clone();
            //this.setVideoTrackContentHints(hintedStream, 'text');
            //this.remoteVideo.srcObject = hintedStream;
            this.media = hintedStream;
        }.bind(this);
    }
    async addIceCandidate(iceCandidate) {
        const candidate = new RTCIceCandidate(iceCandidate);
        //console.log('Received Candidate: ' + iceCandidate);
        await this.peer.addIceCandidate(candidate); 
    }
    async createOffer(){
        const offer = await this.peer.createOffer();
        await this.peer.setLocalDescription(offer);
        return this.peer.localDescription.sdp;
    }
    async processAnswer(sdpAnswer){
        const answer = new RTCSessionDescription({
            type: 'answer',
            sdp: sdpAnswer
          });
        await this.peer.setRemoteDescription(answer);        
    }
    getCapabilitiesAllTracks(stream){
        const videoTracks = stream.getVideoTracks();
        const audioTracks = stream.getAudioTracks();
        videoTracks.forEach((track) => {
            console.log(track.getConstraints());
        })
    }
    setVideoTrackContentHints(stream, hint) {
        const tracks = stream.getVideoTracks();
        tracks.forEach(track => {
          if ('contentHint' in track) {
            console.log('Content Hint Added');
            track.contentHint = hint;
            if (track.contentHint !== hint) {
              console.log('Invalid video track contentHint: \'' + hint + '\'');
            }
          } else {
            console.log('MediaStreamTrack contentHint attribute not supported');
          }
        });
      }
    dispose(){
        if (this.peer.signalingState === 'closed') return;
        else this.peer.close();
    }
}