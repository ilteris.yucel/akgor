module.exports = class User{
    constructor(kurentoClient, sessionId){
        this.kurentoClient = kurentoClient;
        this.sessionId = sessionId;
        this.pipeline = null;
        this.webRtcEndpoint = {
            "480": null,
            "720": null,
            "1080": null
        };
        this.default = this.webRtcEndpoint;
        this.playerEndpoint = null;
        this.localPlayerEndpoint = null;
        this.remotePlayerEndpoint = null;
        this.recorderEndpoint = null;
    }
    getKurentoClient(){
        return this.kurentoClient;
    }
    setKurentoClient(kurentoClient){
        this.kurentoClient = kurentoClient;
    }
    getSessionId(){
        return this.sessionId;
    }
    setSessionId(sessionId){
        this.sessionId = sessionId;
    }
    getPipeline(){
        return this.pipeline;
    }
    setPipeline(pipeline){
        this.pipeline = pipeline;
    }
    getWebRtcEndpoint(resolution){
        return this.webRtcEndpoint[resolution];
    }
    getAllWebRtcEndpoint(){
        return this.webRtcEndpoint;
    }
    setWebRtcEndpoint(webRtcEndpoint, resolution){
        this.webRtcEndpoint[resolution] = webRtcEndpoint;
    }
    setAllWebrtcEndpoint(obj){
        this.webRtcEndpoint = obj;
    }
    getPlayerEndpoint(){
        return this.playerEndpoint;
    }
    setPlayerEndpoint(playerEndpoint){
        this.playerEndpoint = playerEndpoint;
    }
    getLocalPlayerEndpoint(){
        return this.localPlayerEndpoint;
    }
    setLocalPlayerEndpoint(playerEndpoint){
        this.localPlayerEndpoint = playerEndpoint;
    }
    getRemotePlayerEndpoint(){
        return this.remotePlayerEndpoint;
    }
    setRemotePlayerEndpoint(playerEndpoint){
        this.remotePlayerEndpoint = playerEndpoint;
    }
    getRecorderEndpoint(){
        return this.recorderEndpoint;
    }
    setRecorderEndpoint(recorderEndpoint){
        this.recorderEndpoint = recorderEndpoint;
    }
}
