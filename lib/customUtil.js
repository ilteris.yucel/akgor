module.exports = {
    uid: function(){
        return Date.now().toString(36) + Math.random().toString(36).substr(2);
    },
    timeout: function(ms){
        return new Promise(resolve => setTimeout(resolve, ms));
    }
};
